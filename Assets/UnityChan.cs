﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class UnityChan : MonoBehaviour {

	private Animator animator;
	private int doWalkId;
	private int[] pose = new int[8];
	public GameObject Balloon;
	public GameObject Message;

	public GameObject Ayano;

	private string [,] message;

	void Start () {
		animator = GetComponent<Animator> ();
		doWalkId = Animator.StringToHash ("isWalk");

		message = new string[3,3]{{"任天堂信者","やるといったこと\nはやる女","負けず嫌い"}, 
								{"腹痛のプロ","","まじめ "},
								{"孤高のスナイパー","未開の地","人の笑顔が好き"}
								};

		for (int i=0; i<8; i++) {
			pose [i] = Animator.StringToHash ( "pose" + i.ToString());
		}
	}

	void Update () {
		if (Input.GetKey (KeyCode.LeftArrow)) {
			Walk (Vector3.left*3.0f);
		} else if (Input.GetKey (KeyCode.RightArrow)) {
			Walk (Vector3.right*3.0f);
		} else if (Input.GetKey (KeyCode.UpArrow)) {
			Walk (Vector3.up/0.5f + Vector3.forward/0.3f);
		}
		else if (Input.GetKey (KeyCode.DownArrow)) {
			Walk (Vector3.down/0.5f + Vector3.back/0.3f);
		}
		else {
			animator.SetBool (doWalkId, false);
			transform.LookAt (transform.position + Vector3.back);
			TextCheck();
		}
	}

	void Walk(Vector3 direction){
		animator.SetBool (doWalkId, true);
		Balloon.GetComponent<MeshRenderer>().enabled = false;
		Message.GetComponent<TextMesh> ().text = "";

		if (animator.GetCurrentAnimatorStateInfo(0).IsName("Base Layer.Walk") ){
			transform.LookAt (transform.position + direction);
			GetComponent<CharacterController> ().Move (direction / 3.0f);
		}
	}

	void TextCheck(){
		Vector3 vec = (this.transform.position - Ayano.transform.position).normalized;
		int row, col;
		row = col = 100;

		Debug.Log (vec);
		if (vec.x >= -1.1f && vec.x <= -0.3f) {
			row = 0;
		} else if (vec.x >= -0.4f && vec.x <= 0.3f) {
			row = 1;
		} else if (vec.x >= 0.5f && vec.x <= 1.1f) {
			row = 2;
		}

		if (vec.y >= 0.1f && vec.y <= 1.0f) {
			col = 0;
		} else if (vec.y >= -0.2f && vec.y <= 0.0f) {
			col = 1;
		} else if (vec.y >= -1.0f && vec.y <= -0.2f) {
			col = 2;
		}
		Debug.Log ("Row " + row);
		Debug.Log ("Col " + col);
		if (row + col < 100) {
			if(row*col !=1){
				Message.GetComponent<TextMesh> ().text = message [col, row];
				Debug.Log (message[col,row]);
				Balloon.GetComponent<MeshRenderer> ().enabled = true;
			}
		}
		else {
			Message.GetComponent<TextMesh> ().text = "";
			Balloon.GetComponent<MeshRenderer> ().enabled = false;
		}

	}

}
