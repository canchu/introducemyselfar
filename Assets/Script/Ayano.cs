﻿using UnityEngine;
using System.Collections;

public class Ayano : MonoBehaviour {

	public GameObject Camera;
	public GameObject AyanoObj;
	private Vector3 screenPoint;
	private Vector3 worldPoint;
	private Vector3 fromPosition;
	private float rate;
	private bool  moveFlag;

	void Start () {
		moveFlag = false;
		//startTime = Time.timeSinceLevelLoad;
	}

	void Update () {

		if (Input.GetMouseButton(0)) {
			rate = 0.0f;
			screenPoint = Input.mousePosition;
			//screenPoint.z = 500.0f;
			worldPoint = Camera.GetComponent<Camera> ().ScreenToWorldPoint (screenPoint);
			fromPosition = this.transform.position;
			moveFlag = true;
		}

		if (moveFlag) {
			rate += 0.05f;
			this.transform.position = Vector3.Lerp (fromPosition, worldPoint, rate);

			if (this.transform.position == worldPoint) {
				moveFlag = false;
				rate = 0.0f;
			}
		}


	}
}
